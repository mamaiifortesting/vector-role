Vector
=========

Role for install Vector

Role Variables
--------------

vector_config_dir
vector_config

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: vector-role}

License
-------

MIT

Author Information
------------------

Roman Turkish
